#! /bin/bash

# Device name of the tablet retrieved from xsetwacom
DEVICE="Wacom Intuos4 8x13 Pad pad"

# Path to the tablet config application
ICONSPATH=~/Scripts/intuos4-led/icons
TICMD=$ICONSPATH/text-icon.sh
LEDCMD=~/Scripts/intuos4-led/src/intuos4-led-config

# Create images
#(apart from any converted from acutal image rather than text like arrow up)
$TICMD "Undo" undo
mv icon-undo.png $ICONSPATH
$TICMD "Redo" redo
mv icon-redo.png $ICONSPATH
$TICMD "Swap Colour" swap-colour
mv icon-swap-colour.png $ICONSPATH
$TICMD "Mirror Img" mirror-image
mv icon-mirror-image.png $ICONSPATH
$TICMD "Deselect" deselect
mv icon-deselect.png $ICONSPATH
$TICMD "Eraser Mode" eraser-mode
mv icon-eraser-mode.png $ICONSPATH
$TICMD "Save As..." save-as
mv icon-save-as.png $ICONSPATH

#set the OLEDs - button numbers corresond to OLED position top to bottom
sudo rmmod wacom
sudo $LEDCMD --button 1 --image $ICONSPATH/icon-undo.png
sudo $LEDCMD --button 2 --image $ICONSPATH/icon-redo.png
sudo $LEDCMD --button 3 --image $ICONSPATH/icon-swap-colour.png
sudo $LEDCMD --button 4 --image $ICONSPATH/icon-mirror-image.png
sudo $LEDCMD --button 5 --image $ICONSPATH/icon-arrow-up.png #this is not made on the fly
sudo $LEDCMD --button 6 --image $ICONSPATH/icon-deselect.png
sudo $LEDCMD --button 7 --image $ICONSPATH/icon-eraser-mode.png
sudo $LEDCMD --button 8 --image $ICONSPATH/icon-save-as.png
sudo modprobe wacom

sleep 1

#button numbers for driver do not correspond to button numbers for OLED!
#here they are laid out top to bottom
xsetwacom --set "$DEVICE" Button 2 "key ctrl z"
xsetwacom --set "$DEVICE" Button 3 "key ctrl shift z"
xsetwacom --set "$DEVICE" Button 8 "key x"
xsetwacom --set "$DEVICE" Button 9 "key m"
#my big button in the middle has no LED
xsetwacom --set "$DEVICE" Button 1 "key ctrl"

xsetwacom --set "$DEVICE" Button 10  "key shift"
xsetwacom --set "$DEVICE" Button 11 "key ctrl shift a"
xsetwacom --set "$DEVICE" Button 12 "key e"
xsetwacom --set "$DEVICE" Button 13 "key ctrl shift s"
